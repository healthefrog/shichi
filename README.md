# HealthForge Coding Quiz

Hello! Thanks for taking the time to do our technical test. We know these are annoying, but we need a way to assess everyone that we employ to avoid disappointment on both sides later on.

Please follow the instructions below and when you are done, put your code into a git repo you own and send us a link to it for us to assess. GitHub or BitBucket are both free for public repos.

This test shouldn't take more than two or three hours, so if you are taking longer than that, it might be best to stop and discuss what you have managed to do with us.
 
Feel free to use as much Google-Fu as you need to (that is what we do on a daily basis) - however, you need to do the test on your own. Any outside help is not in the spirit of the test and will result in seven or eight years of bad luck, depending on which is worst for you.

# Pre-requesites

You will need:

- a git client
- NodeJS
- an IDE of your choice

# Getting Started

Clone the repo somewhere locally.

Run:

```
yarn
yarn start
```

Open a browser at http://localhost:1234/


# Question 1

Use an asynchronous http request to get the list of patients from http://localhost:1234/patients.json and display them nicely in the table on the main page.

# Question 2

Add a form that adds a new patient to the list kept in memory.

We are expecting a hard refresh or reloading the app will lose any changes.

# Question 3

Add sorting and search to the table.
