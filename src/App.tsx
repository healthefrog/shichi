import * as React from "react";
import { Container, Header, Segment, Table } from "semantic-ui-react";

export const App = () => (
  <Container style={{ marginTop: "3rem" }}>
    <Header>Patient List</Header>
    <Table celled>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>NHS Number</Table.HeaderCell>
          <Table.HeaderCell>First name</Table.HeaderCell>
          <Table.HeaderCell>Last name</Table.HeaderCell>
          <Table.HeaderCell>Date of birth</Table.HeaderCell>
        </Table.Row>
      </Table.Header>

      <Table.Body>
        <Table.Row>
          <Table.Cell>-</Table.Cell>
          <Table.Cell>-</Table.Cell>
          <Table.Cell>-</Table.Cell>
          <Table.Cell>-</Table.Cell>
        </Table.Row>
      </Table.Body>
    </Table>
    <Segment secondary>
      <Header>Add a patient</Header>
      <div>TODO: add form here</div>
    </Segment>
  </Container>
);
