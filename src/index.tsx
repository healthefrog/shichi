import * as React from "react";
import * as ReactDOM from "react-dom";
import { App } from "./App";

if ((module as any).hot) {
  (module as any).hot.accept(() => {
    window.location.reload();
  });
}

ReactDOM.render(<App />, document.getElementById("app"));
